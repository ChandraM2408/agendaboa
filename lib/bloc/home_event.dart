class HomeEvent {}

class WatchHomeEvent extends HomeEvent {}

class ChangeTabEvent extends HomeEvent {
  final int tabId;
  ChangeTabEvent({required this.tabId});
}

class IncrementCounterEvent extends HomeEvent {}

class ClearAllEvent extends HomeEvent {}

class HomeUpdateStreamEvent extends HomeEvent {
  final Map<String, dynamic>? data;
  HomeUpdateStreamEvent({this.data});
}
