import 'package:equatable/equatable.dart';

class HomeState extends Equatable {
  ///Backup counter state so to prevent API call when tab is changed
  final Map<String, int> countMap;
  final int count;
  final int currentPage;

  HomeState({
    this.count = 0,
    this.currentPage = 0,
    this.countMap = const <String, int>{},
  });

  @override
  List<Object?> get props => [
        count,
        currentPage,
        countMap,
      ];
}

class InitialHomeState extends HomeState {}

class LoadedHomeState extends HomeState {
  LoadedHomeState({
    required int count,
    required int currentPage,
    required Map<String, int> countMap,
  }) : super(
          count: count,
          currentPage: currentPage,
          countMap: countMap,
        );
}
