import 'dart:async';

import 'package:agenda_boa_assignment/bloc/home_event.dart';
import 'package:agenda_boa_assignment/bloc/home_state.dart';
import 'package:agenda_boa_assignment/domain/repositories/counter_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  ///This can be replaced with Dependency injection
  CounterRepository _counterRepo = CounterRepositoryImpl();

  HomeBloc() : super(InitialHomeState()) {
    on<ChangeTabEvent>(_reqChangeTab);
    on<WatchHomeEvent>(_reqWatchHome);
    on<IncrementCounterEvent>(_reqIncrementCounter);
    on<HomeUpdateStreamEvent>(_reqHomeUpdate);
    on<ClearAllEvent>(_clearAll);
  }

  void _reqWatchHome(WatchHomeEvent event, Emitter<HomeState> emit) =>
      _counterRepo.watchCounterUpdates().listen((snapshots) {
        add(HomeUpdateStreamEvent(data: snapshots.data()));
      });

  void _reqChangeTab(ChangeTabEvent event, Emitter<HomeState> emit) => emit(
        LoadedHomeState(
          count: state.countMap['page_${event.tabId}']!,
          currentPage: event.tabId,
          countMap: state.countMap,
        ),
      );

  FutureOr<void> _reqHomeUpdate(
    HomeUpdateStreamEvent event,
    Emitter<HomeState> emit,
  ) {
    final countMap = <String, int>{};
    event.data?.forEach(((key, value) => countMap[key] = value));

    emit(
      LoadedHomeState(
        count: event.data?['page_${state.currentPage}'],
        currentPage: state.currentPage,
        countMap: countMap,
      ),
    );
  }

  void _reqIncrementCounter(
          IncrementCounterEvent event, Emitter<HomeState> emit) =>
      _counterRepo.update(state.currentPage, state.count + 1);

  void _clearAll(ClearAllEvent event, Emitter<HomeState> emit) =>
      _counterRepo.delete();
}
