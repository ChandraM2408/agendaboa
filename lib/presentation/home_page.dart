import 'package:agenda_boa_assignment/bloc/home_bloc.dart';
import 'package:agenda_boa_assignment/bloc/home_event.dart';
import 'package:agenda_boa_assignment/bloc/home_state.dart';
import 'package:agenda_boa_assignment/presentation/counter_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      bloc: context.read<HomeBloc>()..add(WatchHomeEvent()),
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text('Agenda Boa'),
            actions: [
              IconButton(
                onPressed: () => context.read<HomeBloc>().add(ClearAllEvent()),
                icon: Icon(Icons.clear_all),
              )
            ],
          ),
          body: BlocBuilder<HomeBloc, HomeState>(
            builder: (context, state) => CounterView(count: state.count),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () =>
                context.read<HomeBloc>().add(IncrementCounterEvent()),
            tooltip: 'Increment',
            child: Icon(Icons.add),
          ), // This trailing comma makes auto-formatting nicer for build methods.
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: state.currentPage,
            onTap: (index) =>
                context.read<HomeBloc>().add(ChangeTabEvent(tabId: index)),
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.pages),
                label: 'Counter 1',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.pages),
                label: 'Counter 2',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.pages),
                label: 'Counter 3',
              ),
            ],
          ),
        );
      },
    );
  }
}
