import 'package:cloud_firestore/cloud_firestore.dart';

abstract class CounterRepository {
  Stream watchCounterUpdates();
  void update(int pageId, int value);
  void delete();
}

class CounterRepositoryImpl implements CounterRepository {
  final _collection = 'counter_map';
  final _documentId = 'counter';
  FirebaseFirestore _database = FirebaseFirestore.instance;

  @override
  void delete() {
    _database.collection(_collection).doc(_documentId).update({
      'page_0': 0,
      'page_1': 0,
      'page_2': 0,
    });
  }

  @override
  void update(int pageId, int value) {
    _database.collection(_collection).doc(_documentId).update({
      'page_$pageId': value,
    });
  }

  @override
  Stream watchCounterUpdates() =>
      _database.collection(_collection).doc(_documentId).snapshots();
}
